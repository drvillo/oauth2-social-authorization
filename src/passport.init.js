const User = require('./models/user')
const {Token} = require('./models/token')
const passport = require('passport')
// const { Strategy: TwitterStrategy } = require('passport-twitter')
const {OAuth2Strategy: GoogleStrategy} = require('passport-google-oauth')
// const { Strategy: FacebookStrategy } = require('passport-facebook')
// const { Strategy: GithubStrategy} = require('passport-github')
const {Strategy: LocalStrategy} = require('passport-local')
const {Strategy: BearerStrategy} = require('passport-http-bearer')
const {BasicStrategy: BasicStrategy} = require('passport-http')
const {Strategy: ClientPasswordStrategy} = require('passport-oauth2-client-password')
const {
	TWITTER_CONFIG, GOOGLE_CONFIG, FACEBOOK_CONFIG, GITHUB_CONFIG, LOCAL_CONFIG
} = require('../config')

module.exports = () => {

	// Allowing passport to serialize and deserialize users into sessions
	passport.serializeUser((user, cb) => cb(null, user))
	passport.deserializeUser((obj, cb) => cb(null, obj))

	// The callback that is invoked when an OAuth provider sends back user
	// information. Normally, you would save the user to the database
	// in this callback and it would be customized for each provider.
	const callback = (accessToken, refreshToken, profile, cb) => cb(null, profile)

	// Adding each OAuth provider's strategy to passport
	// passport.use(new TwitterStrategy(TWITTER_CONFIG, callback))
	passport.use(new GoogleStrategy(GOOGLE_CONFIG, callback))
	// passport.use(new FacebookStrategy(FACEBOOK_CONFIG, callback))
	// passport.use(new GithubStrategy(GITHUB_CONFIG, callback))

	const signupCallback = (req, email, password, done) => {
		const userData = {
			email: email.trim(),
			password: password.trim(),
			name: req.body.name.trim()
		}

		const newUser = new User(userData)
		newUser.save((err) => {
			if (err) {
				return done(err)
			}

			return done(null)
		})
	}
	passport.use('local-signup', new LocalStrategy(LOCAL_CONFIG, signupCallback))

	/**
	 * TODO: add client validation
	 */
	passport.use("clientBasic", new BasicStrategy(
		function (clientId, clientSecret, done) {
			return done(null, clientId)
		}
	))

	/**
	 * TODO: add client validation
	 */
	passport.use("clientPassword", new ClientPasswordStrategy(
		function (clientId, clientSecret, done) {
			return done(null, clientId)
		})
	)

	/**
	 * This strategy is used to authenticate users based on an access token (aka a
	 * bearer token).
	 */
	passport.use("accessToken", new BearerStrategy(
		async function (accessToken, done) {
			try {

				const token = await Token.findOne({token: accessToken})
				if (!token) return done(null, false)
				if (new Date() > token.expiresAt) {
					done(null, false)

				} else {
					const user = await User.findOne({email: token.userId})

					if (!user) return done(null, false)
					// no use of scopes for no
					let info = {scope: '*'}
					done(null, user, info)

				}
			} catch (err) {
				done(err)
			}
		}
	))
}