const mongoose = require('mongoose');

const TokenSchema = new mongoose.Schema({
	token: {
		type: String,
		index: {unique: true}
	},
	expiresAt: {type: Date},
	clientId: String,
	userId: String,
	scope: String
});

RefreshTokenSchema = new mongoose.Schema({
	refreshToken: {
		type: String,
		index: {unique: true}
	},

	clientId: String,
	userId: String,
});




exports.Token = mongoose.model('Token', TokenSchema)
exports.RefreshToken = mongoose.model('RefreshToken', RefreshTokenSchema)
exports.saveTokens = async(t,rt) => {
	const toSave = []
	console.log("saving: ",t)
	toSave.push(t.save())
	toSave.push(rt.save())
	return Promise.all(toSave)
}