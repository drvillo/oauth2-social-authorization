const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// define the User model schema
const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    index: { unique: true }
  },
  password: String,
  name: String
});


/**
 * Compare the passed password with the value in the database. A model method.
 * Awaits so that if th caller does not call this with await it just returns the promise
 * @param {string} password
 */
UserSchema.methods.comparePassword = async function (password) {
  return await bcrypt.compare(password, this.password)
};

UserSchema.methods.getPublicAttributes = function() {
  return {
    email: this.email,
    name: this.name
  }
}

/**
 * The pre-save hook method.
 */
UserSchema.pre('save', function (next) {
  const user = this;

  // proceed further only if the password is modified or the user is new
  if (!user.isModified('password')) return next();

  return bcrypt.genSalt((saltError, salt) => {
    if (saltError) { return next(saltError); }

    return bcrypt.hash(user.password, salt, (hashError, hash) => {
      if (hashError) { return next(hashError); }

      // replace a password string with hash value
      user.password = hash;

      return next();
    });
  });
});


module.exports = mongoose.model('User', UserSchema);