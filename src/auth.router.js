const express = require('express')
const router = express.Router()
const passport = require('passport')
const authController = require('./auth.controller')
const oauth = require('./oauth')
// Setting up the passport middleware for each of the OAuth providers
const twitterAuth = passport.authenticate('twitter')
const googleAuth = passport.authenticate('google', {scope: ['profile']})
const facebookAuth = passport.authenticate('facebook')
const githubAuth = passport.authenticate('github')
const oauth2Auth = passport.authenticate('accessToken', {session: false})

// Routes that are triggered by the callbacks from each OAuth provider once
// the user has authenticated successfully
router.get('/twitter/callback', twitterAuth, authController.twitter)
router.get('/google/callback', googleAuth, authController.google)
router.get('/facebook/callback', facebookAuth, authController.facebook)
router.get('/github/callback', githubAuth, authController.github)

// This custom middleware allows us to attach the socket id to the session
// With that socket id we can send back the right user info to the right 
// socket
router.use((req, res, next) => {
	req.session.socketId = req.query.socketId
	next()
})

// Routes that are triggered on the client
router.get('/twitter', twitterAuth)
router.get('/google', googleAuth)
router.get('/facebook', facebookAuth)
router.get('/github', githubAuth)


//Local Signup


const validator = require('validator')

const validateSignupForm = (payload) => {
	const errors = {}
	let isFormValid = true
	let message = ''

	console.log(payload)
	if (!payload || typeof payload.email !== 'string' || !validator.isEmail(payload.email)) {
		isFormValid = false
		errors.email = 'Please provide a correct email address.'
	}

	if (!payload || typeof payload.password !== 'string' || payload.password.trim().length < 8) {
		isFormValid = false
		errors.password = 'Password must have at least 8 characters.'
	}

	if (!payload || typeof payload.name !== 'string' || payload.name.trim().length === 0) {
		isFormValid = false
		errors.name = 'Please provide your name.'
	}

	if (!isFormValid) {
		message = 'Check the form for errors.'
	}

	return {
		success: isFormValid,
		message,
		errors
	}
}

const validRequest = function (req, res, next) {
	const validationResult = validateSignupForm(req.body)
	if (!validationResult.success) {
		return res.status(400).json({
			success: false,
			message: validationResult.message,
			errors: validationResult.errors
		})
	} else
		next()
}

/**
 * Signup route
 */
router.post('/users', validRequest, (req, res, next) => {


	return passport.authenticate('local-signup', (err) => {
		if (err) {
			if (err.name === 'MongoError' && err.code === 11000) {
				// the 11000 Mongo code is for a duplication email error
				// the 409 HTTP status code is for conflict error
				return res.status(409).json({
					success: false,
					message: 'Check the form for errors.',
					errors: {
						email: 'This email is already taken.'
					}
				})
			}
			console.log(err)
			return res.status(400).json({
				success: false,
				message: 'Could not process the form.'
			})
		}

		return res.status(201).json({
			success: true,
			message: 'You have successfully signed up! Now you should be able to log in.'
		})
	})(req, res, next)
})


router.get('/users/self', oauth2Auth, authController.oauth2)

router.post('/oauth/v2/token', oauth.token)


module.exports = router