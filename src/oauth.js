const oauth2orize = require('oauth2orize')
	, passport = require('passport')
	, User = require('./models/user')
	, {Token, RefreshToken,saveTokens} = require('./models/token')
	, crypto = require('crypto')
	, utils = require("./utils")
	,ONE_HOUR = (3600 * 1000)


const server = oauth2orize.createServer()

//Resource owner password
server.exchange(oauth2orize.exchange.password(async function (client, username, password, scope, done) {

	try {
		const user = await User.findOne({email: username})
		const match = await user.comparePassword(password)
		console.log("match: "+match)
		if (match) {
			const {tokenObject, refreshTokenObject} = buildTokens(client, username, scope)
			await saveTokens(tokenObject,refreshTokenObject)
			done(null, tokenObject.token, refreshTokenObject.refreshToken, {expires_in: tokenObject.expiresAt})
		} else
			return done(null, false)
	} catch (err) {
		return done(err)
	}
}))


const buildTokens = (client, username, scope) => {
	const token = utils.uid(256)
	const refreshToken = utils.uid(256)
	const tokenHash = crypto.createHash('sha1').update(token).digest('hex')
	const refreshTokenHash = crypto.createHash('sha1').update(refreshToken).digest('hex')

	const expiresAt = new Date(new Date().getTime() + ONE_HOUR)

	const tokenObject = new Token({
		token: tokenHash,
		expiresAt: expiresAt,
		clientId: client.clientId,
		userId: username,
		scope: scope
	})

	const refreshTokenObject = new RefreshToken({
			refreshToken: refreshTokenHash,
			clientId: client.clientId,
			userId: username
		}

	)
	return {tokenObject,refreshTokenObject}
}

// token endpoint
exports.token = [
	passport.authenticate(['clientBasic', 'clientPassword'], {session: false}),
	server.token(),
	server.errorHandler()
]